<?php

/*
 * This file is part of the Hermes\HttpKernel library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\HttpKernel;

use Hermes\Pipeline\Loader\MiddlewareLoader;
use Hermes\Pipeline\Pipeline;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Zend\Expressive\Router\Route;
use Zend\Expressive\Router\RouteCollector;

/**
 * This HTTP Kernel the minimum required to build a web application.
 *
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
final class HttpKernel implements RequestHandlerInterface, MiddlewareInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var Pipeline
     */
    private $pipeline;
    /**
     * @var MiddlewareLoader
     */
    private $middlewareLoader;
    /**
     * @var RouteCollector
     */
    private $routeCollector;
    /**
     * @var callable|null
     */
    private $exceptionHandler;

    /**
     * HttpKernel constructor.
     *
     * @param Pipeline         $pipeline
     * @param MiddlewareLoader $middlewareLoader
     * @param RouteCollector   $routeCollector
     * @param callable|null    $exceptionHandler
     */
    public function __construct(
        Pipeline $pipeline,
        MiddlewareLoader $middlewareLoader,
        RouteCollector $routeCollector,
        callable $exceptionHandler = null
    ) {
        $this->pipeline = $pipeline;
        $this->middlewareLoader = $middlewareLoader;
        $this->routeCollector = $routeCollector;
        $this->exceptionHandler = $exceptionHandler;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     *
     * @throws \Throwable
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $start = microtime(true);

        $this->logger && $this->logger->debug('New request has been received', [
            'method' => $request->getMethod(),
            'path' => $request->getUri()->getPath(),
            'at' => date(DATE_ATOM),
        ]);

        try {
            $response = $this->pipeline->handle($request);
        } catch (\Throwable $exception) {
            $this->logger && $this->logger->debug(sprintf('An error has occurred: %s', $exception->getMessage()));
            if ($this->exceptionHandler) {
                return ($this->exceptionHandler)($exception);
            }
            throw $exception;
        }

        $this->logger && $this->logger->debug('Request handled successfully.', [
            'max_memory' => memory_get_peak_usage(),
            'time' => microtime(true) - $start,
        ]);

        return $response;
    }

    /**
     * @param callable $exceptionHandler
     */
    public function setExceptionHandler(callable $exceptionHandler): void
    {
        $this->exceptionHandler = $exceptionHandler;
    }

    /**
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     *
     * @throws \Throwable
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $this->handle($request);
    }

    /**
     * @param $middleware
     */
    public function pipe($middleware): void
    {
        $this->pipeline->pipe($this->middlewareLoader->load($middleware));
    }

    /**
     * @param string                                                            $path
     * @param string|array|callable|MiddlewareInterface|RequestHandlerInterface $handler
     * @param null|string                                                       $name    the name of the route
     *
     * @return Route
     */
    public function get(string $path, $handler, string $name = null): Route
    {
        return $this->routeCollector->get($path, $this->middlewareLoader->load($handler), $name);
    }

    /**
     * @param string                                                            $path
     * @param string|array|callable|MiddlewareInterface|RequestHandlerInterface $handler
     * @param null|string                                                       $name    the name of the route
     *
     * @return Route
     */
    public function post(string $path, $handler, string $name = null): Route
    {
        return $this->routeCollector->post($path, $this->middlewareLoader->load($handler), $name);
    }

    /**
     * @param string                                                            $path
     * @param string|array|callable|MiddlewareInterface|RequestHandlerInterface $handler
     * @param null|string                                                       $name    the name of the route
     *
     * @return Route
     */
    public function put(string $path, $handler, string $name = null): Route
    {
        return $this->routeCollector->put($path, $this->middlewareLoader->load($handler), $name);
    }

    /**
     * @param string                                                            $path
     * @param string|array|callable|MiddlewareInterface|RequestHandlerInterface $handler
     * @param null|string                                                       $name    the name of the route
     *
     * @return Route
     */
    public function patch(string $path, $handler, string $name = null): Route
    {
        return $this->routeCollector->patch($path, $this->middlewareLoader->load($handler), $name);
    }

    /**
     * @param string                                                            $path
     * @param string|array|callable|MiddlewareInterface|RequestHandlerInterface $handler
     * @param null|string                                                       $name    the name of the route
     *
     * @return Route
     */
    public function delete(string $path, $handler, string $name = null): Route
    {
        return $this->routeCollector->delete($path, $this->middlewareLoader->load($handler), $name);
    }

    /**
     * @param string                                                            $path
     * @param string|array|callable|MiddlewareInterface|RequestHandlerInterface $handler
     * @param null|string                                                       $name    the name of the route
     *
     * @return Route
     */
    public function any(string $path, $handler, string $name = null): Route
    {
        return $this->routeCollector->any($path, $this->middlewareLoader->load($handler), $name);
    }
}
