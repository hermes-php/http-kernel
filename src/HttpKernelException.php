<?php

/*
 * This file is part of the Hermes\HttpKernel library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\HttpKernel;

/**
 * Class HttpKernelException.
 *
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class HttpKernelException extends \RuntimeException
{
    /**
     * @param string $serviceName
     *
     * @return HttpKernelException
     */
    public static function requiredServiceMissing(string $serviceName): HttpKernelException
    {
        return new self(sprintf('Cannot instantiate %s. The required %s service is missing', HttpKernel::class, $serviceName));
    }
}
