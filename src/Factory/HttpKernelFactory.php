<?php

/*
 * This file is part of the Hermes\HttpKernel library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\HttpKernel\Factory;

use Hermes\HttpKernel\HttpKernel;
use Hermes\HttpKernel\HttpKernelException;
use Hermes\Pipeline\Loader\MiddlewareLoader;
use Hermes\Pipeline\Pipeline;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Zend\Expressive\Router\RouteCollector;

/**
 * Class HttpKernelFactory.
 *
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class HttpKernelFactory
{
    /**
     * @var string
     */
    private $pipelineService;
    /**
     * @var string
     */
    private $middlewareLoaderService;
    /**
     * @var string
     */
    private $routeCollectorService;

    /**
     * HttpKernelFactory constructor.
     *
     * @param string $pipelineService
     * @param string $middlewareLoaderService
     * @param string $routeCollectorService
     */
    public function __construct(
        string $pipelineService = Pipeline::class,
        string $middlewareLoaderService = MiddlewareLoader::class,
        string $routeCollectorService = RouteCollector::class
    ) {
        $this->pipelineService = $pipelineService;
        $this->middlewareLoaderService = $middlewareLoaderService;
        $this->routeCollectorService = $routeCollectorService;
    }

    /**
     * @param ContainerInterface $container
     *
     * @return HttpKernel
     */
    public function __invoke(ContainerInterface $container): HttpKernel
    {
        if (!$container->has($this->pipelineService)) {
            throw HttpKernelException::requiredServiceMissing($this->pipelineService);
        }
        if (!$container->has($this->middlewareLoaderService)) {
            throw HttpKernelException::requiredServiceMissing($this->middlewareLoaderService);
        }
        if (!$container->has($this->routeCollectorService)) {
            throw HttpKernelException::requiredServiceMissing($this->routeCollectorService);
        }

        $kernel = new HttpKernel(
            $container->get($this->pipelineService),
            $container->get($this->middlewareLoaderService),
            $container->get($this->routeCollectorService)
        );

        if ($container->has(LoggerInterface::class)) {
            $kernel->setLogger($container->get(LoggerInterface::class));
        }

        return $kernel;
    }
}
